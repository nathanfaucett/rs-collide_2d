use vec2;
use number_traits::Float;

use super::BoundingVolume2D;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct BoundingCircle<T>
where
    T: Copy + Float,
{
    position: [T; 2],
    radius: T,
}

impl<T> Default for BoundingCircle<T>
where
    T: Copy + Float,
{
    #[inline(always)]
    fn default() -> Self {
        BoundingCircle {
            position: [T::zero(); 2],
            radius: T::zero(),
        }
    }
}

impl<T> From<([T; 2], T)> for BoundingCircle<T>
where
    T: Copy + Float,
{
    #[inline(always)]
    fn from((position, radius): ([T; 2], T)) -> Self {
        BoundingCircle {
            position: position,
            radius: radius,
        }
    }
}

impl<T> BoundingCircle<T>
where
    T: Copy + Float,
{
    #[inline(always)]
    pub fn set_radius(&mut self, radius: T) {
        self.radius = radius;
    }
}

impl<T> BoundingVolume2D<T> for BoundingCircle<T>
where
    T: Copy + Float,
{
    #[inline(always)]
    fn radius(&self) -> T {
        self.radius
    }
    #[inline]
    fn set_center(&mut self, center: &[T; 2]) {
        vec2::copy(&mut self.position, center);
    }

    #[inline]
    fn intersects(&self, other: &Self) -> bool {
        let dx = other.position[0] - self.position[0];
        let dy = other.position[1] - self.position[1];
        let r = other.radius + self.radius;
        let rsq = r * r;
        let dsq = dx * dx + dy * dy;
        dsq <= rsq
    }

    #[inline]
    fn from_center_size(&mut self, center: &[T; 2], size: &[T; 2]) {
        self.radius = vec2::length(size) / T::from_usize(2);
        self.position[0] = center[0];
        self.position[1] = center[1];
    }
    #[inline]
    fn from_center_radius(&mut self, center: &[T; 2], radius: T) {
        self.radius = radius;
        self.position[0] = center[0];
        self.position[1] = center[1];
    }
    #[inline]
    fn expand_point(&mut self, point: &[T; 2]) {
        let dx = point[0] - self.position[0];
        let dy = point[1] - self.position[1];
        let r = self.radius;
        let rsq = r * r;
        let dsq = dx * dx + dy * dy;

        if dsq < rsq {
            self.radius = dsq.sqrt();
        }
    }
}
