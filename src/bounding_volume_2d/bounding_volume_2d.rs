use number_traits::Float;

pub trait BoundingVolume2D<T>: Default
where
    T: Copy + Float,
{
    fn radius(&self) -> T;
    fn set_center(&mut self, center: &[T; 2]);
    fn intersects(&self, &Self) -> bool;
    fn from_center_size(&mut self, center: &[T; 2], size: &[T; 2]);
    fn from_center_radius(&mut self, center: &[T; 2], radius: T);
    fn expand_point(&mut self, point: &[T; 2]);
}
