use aabb2::{self, AABB2};
use number_traits::Float;

use super::BoundingVolume2D;

impl<T> BoundingVolume2D<T> for AABB2<T>
where
    T: Copy + Float,
{
    #[inline(always)]
    fn radius(&self) -> T {
        let w = self.max[0] - self.min[0];
        let h = self.max[1] - self.min[1];
        let lsq = w * w + h * h;

        if lsq > T::zero() {
            lsq.sqrt() / T::from_usize(2)
        } else {
            T::zero()
        }
    }
    #[inline(always)]
    fn set_center(&mut self, center: &[T; 2]) {
        let w = self.max[0] - self.min[0];
        let h = self.max[1] - self.min[1];
        self.from_center_size(center, &[w, h])
    }
    #[inline(always)]
    fn intersects(&self, other: &Self) -> bool {
        aabb2::intersects(self, other)
    }
    #[inline(always)]
    fn from_center_size(&mut self, center: &[T; 2], size: &[T; 2]) {
        aabb2::from_center_size(self, center, size);
    }
    #[inline(always)]
    fn from_center_radius(&mut self, center: &[T; 2], radius: T) {
        aabb2::from_center_radius(self, center, radius);
    }
    #[inline(always)]
    fn expand_point(&mut self, point: &[T; 2]) {
        let tmp = self.clone();
        aabb2::expand_point(self, &tmp, point);
    }
}
