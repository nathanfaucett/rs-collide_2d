use number_traits::Float;

#[inline]
pub fn circle_to_capsule<T>(
    circle_offset: &[T; 2],
    circle_radius: T,
    capsule_start: &[T; 2],
    capsule_end: &[T; 2],
    capsule_radius: T,
) -> Option<(T, [T; 2], [T; 2])>
where
    T: Copy + Float,
{
    let ll_x = capsule_end[0] - capsule_start[0];
    let ll_y = capsule_end[1] - capsule_start[1];

    let sp_x = circle_offset[0] - capsule_start[0];
    let sp_y = circle_offset[1] - capsule_start[1];

    let len_sq = ll_x * ll_x + ll_y * ll_y;
    let dot_sp = ll_x * sp_x + ll_y * sp_y;
    let t = T::zero().max(&len_sq.min(&dot_sp)) / len_sq;

    let cp_x = capsule_start[0] + t * ll_x;
    let cp_y = capsule_start[1] + t * ll_y;

    let d_x = cp_x - circle_offset[0];
    let d_y = cp_y - circle_offset[1];
    let r = capsule_radius + circle_radius;
    let r_sq = r * r;
    let d_sq = d_x * d_x + d_y * d_y;

    if d_sq < r_sq {
        let d = if d_sq.is_zero() {
            d_sq
        } else {
            d_sq.sqrt()
        };
        let depth = r - d;
        let mut n_x = T::zero();
        let mut n_y = T::one();

        if !d.is_zero() {
            let inv_d = T::one() / d;
            n_x = d_x * inv_d;
            n_y = d_y * inv_d;
        }

        Some((
            depth,
            [
                circle_offset[0] + (n_x * circle_radius),
                circle_offset[1] + (n_y * circle_radius),
            ],
            [n_x, n_y],
        ))
    } else {
        None
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn contact_test() {
        let (depth, point, normal) =
            circle_to_capsule(&[0.0, 1.0], 0.75, &[-1.0, 0.0], &[1.0, 0.0], 0.5).unwrap();
        assert_eq!(depth, 0.25);
        assert_eq!(point, [0.0, 0.25]);
        assert_eq!(normal, [0.0, -1.0]);
    }

    #[test]
    fn no_contact_test() {
        let contact = circle_to_capsule(&[0.0, 1.0], 0.5, &[-1.0, 0.0], &[1.0, 0.0], 0.5);
        assert!(contact.is_none());
    }
}
