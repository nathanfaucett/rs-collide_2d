use number_traits::Float;
use vec2;

#[inline]
pub fn circle_to_circle<T>(
    circle_i_offset: &[T; 2],
    circle_i_radius: T,
    circle_j_offset: &[T; 2],
    circle_j_radius: T,
) -> Option<(T, [T; 2], [T; 2])>
where
    T: Copy + Float,
{
    let d_x = circle_j_offset[0] - circle_i_offset[0];
    let d_y = circle_j_offset[1] - circle_i_offset[1];
    let r = circle_i_radius + circle_j_radius;
    let r_sq = r * r;
    let d_sq = d_x * d_x + d_y * d_y;

    if d_sq < r_sq {
        let mut point = [T::zero(); 2];
        let mut normal = [T::zero(); 2];
        let d;
        let n_x;
        let n_y;

        if d_sq != T::zero() {
            d = d_sq.sqrt();
            let inv_d = T::one() / d;

            n_x = d_x * inv_d;
            n_y = d_y * inv_d;
        } else {
            d = T::zero();
            n_x = T::zero();
            n_y = T::one();
        }

        vec2::set(
            &mut point,
            circle_i_offset[0] + (n_x * circle_i_radius),
            circle_i_offset[1] + (n_y * circle_i_radius),
        );
        vec2::set(&mut normal, n_x, n_y);
        let depth = r - d;

        Some((depth, point, normal))
    } else {
        None
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn contact_test() {
        let (depth, point, normal) = circle_to_circle(&[0.0, 0.0], 0.5, &[0.0, 0.5], 0.5).unwrap();
        assert_eq!(depth, 0.5);
        assert_eq!(point, [0.0, 0.5]);
        assert_eq!(normal, [0.0, 1.0]);
    }

    #[test]
    fn no_contact_test() {
        let contact = circle_to_circle(&[0.0, 0.0], 0.5, &[0.0, 1.0], 0.5);
        assert!(contact.is_none());
    }
}
