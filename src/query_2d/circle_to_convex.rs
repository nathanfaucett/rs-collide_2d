use polygon2::closest_edge_offset_angle;
use number_traits::Float;

use super::circle_to_circle;

#[inline]
pub fn circle_to_convex<T>(
    circle_offset: &[T; 2],
    circle_radius: T,
    convex_offset: &[T; 2],
    convex_angle: T,
    vertices: &[[T; 2]],
) -> Option<(T, [T; 2], [T; 2])>
where
    T: Copy + Float,
{
    let intersection =
        closest_edge_offset_angle(circle_offset, convex_offset, convex_angle, vertices);

    if let Some((depth, point, mut normal)) =
        circle_to_circle(&intersection.point, T::zero(), circle_offset, circle_radius)
    {
        normal[0] = -normal[0];
        normal[1] = -normal[1];
        Some((depth, point, normal))
    } else {
        None
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn contact_test() {
        let (depth, point, normal) = circle_to_convex(
            &[0.0, 1.0],
            0.75,
            &[0.0, 0.0],
            0.0,
            &[[0.5, -0.5], [0.5, 0.5], [-0.5, 0.5], [-0.5, -0.5]],
        ).unwrap();
        assert_eq!(depth, 0.25);
        assert_eq!(point, [0.0, 0.5]);
        assert_eq!(normal, [0.0, -1.0]);
    }

    #[test]
    fn no_contact_test() {
        let contact = circle_to_convex(
            &[0.0, 2.0],
            0.75,
            &[0.0, 0.0],
            0.0,
            &[[0.5, -0.5], [0.5, 0.5], [-0.5, 0.5], [-0.5, -0.5]],
        );
        assert!(contact.is_none());
    }
}
