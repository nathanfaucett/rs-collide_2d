use std::hash::Hash;

use number_traits::Float;

use super::super::{BoundingVolume2D, Shape2D};

pub trait BroadPhase2D<I, T, B>: Default
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
    B: BoundingVolume2D<T>,
{
    #[inline]
    fn run(
        &mut self,
        bounding_volumes: &[(I, &[T; 2], T, &B, &Box<Shape2D<T, B>>)],
    ) -> Vec<((I, usize), (I, usize))> {
        let len = bounding_volumes.len();
        let mut collisions = Vec::new();

        for i in 0..len {
            let &(a_id, _, _, a_bounding_volume, _) = &bounding_volumes[i];

            let mut j = 0;
            while j != i {
                let &(b_id, _, _, b_bounding_volume, _) = &bounding_volumes[j];

                if a_bounding_volume.intersects(b_bounding_volume) {
                    collisions.push(((a_id, i), (b_id, j)));
                }

                j += 1;
            }
        }

        collisions
    }
}
