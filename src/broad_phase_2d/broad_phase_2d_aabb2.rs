use std::hash::Hash;
use std::marker::PhantomData;

use aabb2::AABB2;
use number_traits::Float;

use super::BroadPhase2D;

pub struct BroadPhase2DAABB2<I, T>
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
{
    _marker: PhantomData<(I, T)>,
}

unsafe impl<I, T> Send for BroadPhase2DAABB2<I, T>
where
    I: Send + Copy + Hash + Ord + Eq,
    T: Send + Copy + Float,
{
}
unsafe impl<I, T> Sync for BroadPhase2DAABB2<I, T>
where
    I: Sync + Copy + Hash + Ord + Eq,
    T: Sync + Copy + Float,
{
}

impl<I, T> Default for BroadPhase2DAABB2<I, T>
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
{
    #[inline(always)]
    fn default() -> Self {
        BroadPhase2DAABB2 {
            _marker: PhantomData,
        }
    }
}

impl<I, T> BroadPhase2DAABB2<I, T>
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
{
    #[inline(always)]
    pub fn new() -> Self {
        Self::default()
    }
}

impl<I, T> BroadPhase2D<I, T, AABB2<T>> for BroadPhase2DAABB2<I, T>
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
{
}

#[cfg(test)]
mod test {
    use super::*;

    use aabb2::{self, AABB2};
    use vec2;
    use {Circle, Shape2D};

    fn create_circle<T>(
        id: usize,
        position: [T; 2],
        shape: Circle<T>,
    ) -> (usize, [T; 2], T, AABB2<T>, Box<Shape2D<T, AABB2<T>>>)
    where
        T: 'static + Send + Sync + Copy + Float,
    {
        let mut aabb = aabb2::new_identity();
        let r = shape.radius();
        vec2::set(&mut aabb.min, position[0] - r, position[1] - r);
        vec2::set(&mut aabb.max, position[0] + r, position[1] + r);
        (id, position, T::zero(), aabb, Box::new(shape))
    }

    #[test]
    fn test() {
        let mut broad_phase = BroadPhase2DAABB2::new();

        let owned_objects = vec![
            create_circle(10, [0.0, 0.0], Circle::new(0.5)),
            create_circle(20, [1.0, 0.0], Circle::new(0.5)),
            create_circle(30, [3.0, 0.0], Circle::new(0.5)),
            create_circle(40, [4.0, 0.0], Circle::new(0.5)),
        ];
        let objects: Vec<_> = owned_objects
            .iter()
            .map(|&(id, ref position, rotation, ref aabb, ref shape)| {
                (id, position, rotation, aabb, shape)
            })
            .collect();

        let collisions = broad_phase.run(&*objects);

        assert_eq!(collisions.len(), 2);

        let &((_, a_index), (_, b_index)) = &collisions[0];
        let &(_, _, _, a_aabb, _) = &objects[a_index];
        let &(_, _, _, b_aabb, _) = &objects[b_index];
        assert_eq!(a_aabb, &aabb2::new([0.5, -0.5], [1.5, 0.5]));
        assert_eq!(b_aabb, &aabb2::new([-0.5, -0.5], [0.5, 0.5]));

        let &((_, a_index), (_, b_index)) = &collisions[1];
        let &(_, _, _, a_aabb, _) = &objects[a_index];
        let &(_, _, _, b_aabb, _) = &objects[b_index];
        assert_eq!(a_aabb, &aabb2::new([3.5, -0.5], [4.5, 0.5]));
        assert_eq!(b_aabb, &aabb2::new([2.5, -0.5], [3.5, 0.5]));
    }
}
