use std::hash::Hash;
use std::marker::PhantomData;

use number_traits::Float;

use super::super::BoundingCircle;
use super::BroadPhase2D;

pub struct BroadPhase2DCircle<I, T>
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
{
    _marker: PhantomData<(I, T)>,
}

unsafe impl<I, T> Send for BroadPhase2DCircle<I, T>
where
    I: Send + Copy + Hash + Ord + Eq,
    T: Send + Copy + Float,
{
}
unsafe impl<I, T> Sync for BroadPhase2DCircle<I, T>
where
    I: Sync + Copy + Hash + Ord + Eq,
    T: Sync + Copy + Float,
{
}

impl<I, T> Default for BroadPhase2DCircle<I, T>
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
{
    #[inline(always)]
    fn default() -> Self {
        BroadPhase2DCircle {
            _marker: PhantomData,
        }
    }
}

impl<I, T> BroadPhase2DCircle<I, T>
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
{
    #[inline(always)]
    pub fn new() -> Self {
        Self::default()
    }
}

impl<I, T> BroadPhase2D<I, T, BoundingCircle<T>> for BroadPhase2DCircle<I, T>
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
{
}

#[cfg(test)]
mod test {
    use super::*;

    use {BoundingCircle, BoundingVolume2D, Circle, Shape2D};

    fn create_circle<T>(
        id: usize,
        position: [T; 2],
        shape: Circle<T>,
    ) -> (
        usize,
        [T; 2],
        T,
        BoundingCircle<T>,
        Box<Shape2D<T, BoundingCircle<T>>>,
    )
    where
        T: 'static + Send + Sync + Copy + Float,
    {
        let mut bounding_circle = BoundingCircle::default();
        let r = shape.radius();
        bounding_circle.set_center(&position);
        bounding_circle.set_radius(r);
        (id, position, T::zero(), bounding_circle, Box::new(shape))
    }

    #[test]
    fn test() {
        let mut broad_phase = BroadPhase2DCircle::new();

        let owned_objects = vec![
            create_circle(10, [0.0, 0.0], Circle::new(0.5)),
            create_circle(20, [1.0, 0.0], Circle::new(0.5)),
            create_circle(30, [3.0, 0.0], Circle::new(0.5)),
            create_circle(40, [4.0, 0.0], Circle::new(0.5)),
        ];
        let objects: Vec<_> = owned_objects
            .iter()
            .map(
                |&(id, ref position, rotation, ref bounding_circle, ref shape)| {
                    (id, position, rotation, bounding_circle, shape)
                },
            )
            .collect();

        let collisions = broad_phase.run(&*objects);

        assert_eq!(collisions.len(), 2);

        let &((_, a_index), (_, b_index)) = &collisions[0];
        let &(_, _, _, a_aabb, _) = &objects[a_index];
        let &(_, _, _, b_aabb, _) = &objects[b_index];
        assert_eq!(a_aabb, &BoundingCircle::from(([1.0, 0.0], 0.5)));
        assert_eq!(b_aabb, &BoundingCircle::from(([0.0, 0.0], 0.5)));

        let &((_, a_index), (_, b_index)) = &collisions[1];
        let &(_, _, _, a_aabb, _) = &objects[a_index];
        let &(_, _, _, b_aabb, _) = &objects[b_index];
        assert_eq!(a_aabb, &BoundingCircle::from(([4.0, 0.0], 0.5)));
        assert_eq!(b_aabb, &BoundingCircle::from(([3.0, 0.0], 0.5)));
    }
}
