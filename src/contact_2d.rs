use number_traits::Float;

#[derive(Debug)]
pub struct Contact2D<I, T>
where
    I: Copy + PartialEq + Eq,
    T: Copy + Float,
{
    id_i: I,
    index_i: usize,
    id_j: I,
    index_j: usize,
    depth: T,
    normal: [T; 2],
    point: [T; 2],
}

impl<I, T> Contact2D<I, T>
where
    I: Copy + PartialEq + Eq,
    T: Copy + Float,
{
    #[inline(always)]
    pub fn new(
        id_i: I,
        index_i: usize,
        id_j: I,
        index_j: usize,
        depth: T,
        normal: [T; 2],
        point: [T; 2],
    ) -> Self {
        Contact2D {
            id_i: id_i,
            index_i: index_i,
            id_j: id_j,
            index_j: index_j,
            depth: depth,
            normal: normal,
            point: point,
        }
    }

    #[inline(always)]
    pub fn id_i(&self) -> I {
        self.id_i
    }
    #[inline(always)]
    pub fn index_i(&self) -> usize {
        self.index_i
    }

    #[inline(always)]
    pub fn id_j(&self) -> I {
        self.id_j
    }
    #[inline(always)]
    pub fn index_j(&self) -> usize {
        self.index_j
    }

    #[inline(always)]
    pub fn depth(&self) -> T {
        self.depth
    }
    #[inline(always)]
    pub fn normal(&self) -> &[T; 2] {
        &self.normal
    }
    #[inline(always)]
    pub fn point(&self) -> &[T; 2] {
        &self.point
    }
}
