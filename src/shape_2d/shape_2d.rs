use std::any::{Any, TypeId};

use aabb2::AABB2;
use number_traits::Float;

use super::super::BoundingVolume2D;

pub trait Shape2D<T, B = AABB2<T>>: Any
where
    T: Copy + Float,
    B: BoundingVolume2D<T>,
{
    fn area(&self) -> T;
    fn inertia(&self) -> T;
    fn bounding_volume(&self, bv: &mut B, position: &[T; 2], rotation: T);
}

impl<T, B> Shape2D<T, B>
where
    T: 'static + Copy + Float,
    B: 'static + BoundingVolume2D<T>,
{
    #[inline]
    pub fn is<A>(&self) -> bool
    where
        A: 'static + Shape2D<T, B>,
    {
        let t = TypeId::of::<A>();
        let boxed = self.get_type_id();
        t == boxed
    }

    #[inline]
    pub fn downcast_ref<A>(&self) -> Option<&A>
    where
        A: 'static + Shape2D<T, B>,
    {
        if self.is::<A>() {
            unsafe { Some(&*(self as *const Shape2D<T, B> as *const A)) }
        } else {
            None
        }
    }

    #[inline]
    pub fn downcast_mut<A>(&mut self) -> Option<&mut A>
    where
        A: 'static + Shape2D<T, B>,
    {
        if self.is::<A>() {
            unsafe { Some(&mut *(self as *mut Shape2D<T, B> as *mut A)) }
        } else {
            None
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use aabb2;
    use super::super::Circle;

    #[test]
    fn test() {
        let circle = Circle::new(0.5);
        let mut aabb = aabb2::new_identity();
        circle.bounding_volume(&mut aabb, &[0.5, 0.5], 0.0);
        assert_eq!(aabb.min, [0.0, 0.0]);
        assert_eq!(aabb.max, [1.0, 1.0]);
    }
}
