use number_traits::Float;

use super::super::BoundingVolume2D;
use super::Shape2D;

#[derive(Debug)]
pub struct Circle<T>
where
    T: Copy + Float,
{
    radius: T,
}

impl<T> Circle<T>
where
    T: Copy + Float,
{
    #[inline(always)]
    pub fn new(radius: T) -> Self {
        Circle { radius: radius }
    }

    #[inline(always)]
    pub fn radius(&self) -> T {
        self.radius
    }
}

impl<T, B> Shape2D<T, B> for Circle<T>
where
    T: 'static + Copy + Float,
    B: 'static + BoundingVolume2D<T>,
{
    #[inline(always)]
    fn area(&self) -> T {
        T::PI() * self.radius * self.radius
    }
    #[inline(always)]
    fn inertia(&self) -> T {
        self.radius * self.radius * T::from_f32(0.5)
    }
    #[inline]
    fn bounding_volume(&self, bounding_volume: &mut B, position: &[T; 2], _rotation: T) {
        bounding_volume.from_center_radius(position, self.radius);
    }
}
