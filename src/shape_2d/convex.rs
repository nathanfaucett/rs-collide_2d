use polygon2;
use vec2;
use number_traits::Float;

use super::super::BoundingVolume2D;
use super::Shape2D;

#[derive(Debug)]
pub struct Convex<T>
where
    T: Copy + Float,
{
    vertices: Vec<[T; 2]>,
    normals: Vec<[T; 2]>,
    triangles: Vec<usize>,
}

impl<T> Convex<T>
where
    T: Copy + Float,
{
    /// ```
    /// extern crate collide_2d;
    ///
    /// use collide_2d::Convex;
    ///
    /// fn main() {
    ///     let convex = Convex::new_box(1.0, 1.0);
    ///     assert_eq!(convex.vertices(), &[[0.5, -0.5], [0.5, 0.5], [-0.5, 0.5], [-0.5, -0.5]]);
    ///     assert_eq!(convex.normals(), &[[1.0, 0.0], [0.0, 1.0], [-1.0, 0.0], [0.0, -1.0]]);
    ///     assert_eq!(convex.triangles(), &[0, 1, 2, 0, 2, 3]);
    /// }
    /// ```
    #[inline]
    pub fn new(vertices: Vec<[T; 2]>) -> Self {
        let mut convex = Convex {
            triangles: polygon2::triangulate(&*vertices),
            normals: vertices.clone(),
            vertices: vertices,
        };
        convex.update_normals();
        convex
    }

    #[inline]
    pub fn new_box(w: T, h: T) -> Self {
        let mut vertices = Vec::with_capacity(4);
        let hw = w / T::from_usize(2);
        let hh = h / T::from_usize(2);

        vertices.push([hw, -hh]);
        vertices.push([hw, hh]);
        vertices.push([-hw, hh]);
        vertices.push([-hw, -hh]);

        Self::new(vertices)
    }

    #[inline(always)]
    pub fn update_normals(&mut self) {
        let n = self.vertices.len();
        let n_minus_1 = n - 1;
        let mut normal = [T::zero(); 2];

        for i in 0..n {
            let v1 = &self.vertices[i];
            let v2 = if i == n_minus_1 {
                &self.vertices[0]
            } else {
                &self.vertices[i + 1]
            };

            // rotate clockwise 90 degs
            normal[0] = v2[1] - v1[1];
            normal[1] = -(v2[0] - v1[0]);

            vec2::normalize(&mut self.normals[i], &normal);
        }
    }

    #[inline(always)]
    pub fn vertices(&self) -> &[[T; 2]] {
        &*self.vertices
    }
    #[inline(always)]
    pub fn normals(&self) -> &[[T; 2]] {
        &*self.normals
    }
    #[inline(always)]
    pub fn triangles(&self) -> &[usize] {
        &*self.triangles
    }
}

impl<T, B> Shape2D<T, B> for Convex<T>
where
    T: 'static + Copy + Float,
    B: 'static + BoundingVolume2D<T>,
{
    #[inline(always)]
    fn area(&self) -> T {
        polygon2::area(&*self.vertices)
    }
    #[inline]
    fn inertia(&self) -> T {
        let mut denom = T::zero();
        let mut numer = T::zero();
        let n = self.vertices.len();

        let mut i = 0;
        let mut j = n - 1;
        while i < n {
            let p0 = &self.vertices[j];
            let p1 = &self.vertices[i];

            let a = (vec2::cross(p0, p1)).abs();
            let b = vec2::dot(p1, p1) + vec2::dot(p1, p0) + vec2::dot(p0, p0);

            denom += a * b;
            numer += a;

            j = i;
            i += 1;
        }

        (T::one() / T::from_usize(6)) * (denom / numer)
    }
    /// ```
    /// extern crate collide_2d;
    /// extern crate aabb2;
    ///
    /// use aabb2::AABB2;
    /// use collide_2d::{Shape2D, Convex};
    ///
    /// fn main() {
    ///     let b = Convex::new_box(1.0, 1.0);
    ///     let mut bv = AABB2::default();
    ///     b.bounding_volume(&mut bv, &[0.0, 0.0], 0.0);
    ///     assert_eq!(&bv.min, &[-0.5, -0.5]);
    ///     assert_eq!(&bv.max, &[0.5, 0.5]);
    /// }
    /// ```
    #[inline]
    fn bounding_volume(&self, bounding_volume: &mut B, position: &[T; 2], rotation: T) {
        let mut t1 = [T::zero(); 2];
        let mut t2 = [T::zero(); 2];

        bounding_volume.set_center(position);

        for vertex in &self.vertices {
            vec2::transform_angle(&mut t1, vertex, rotation);
            vec2::add(&mut t2, position, &t1);
            bounding_volume.expand_point(&t2);
        }
    }
}
