use number_traits::Float;
use vec2;

use super::super::BoundingVolume2D;
use super::Shape2D;

#[derive(Debug)]
pub struct Line<T>
where
    T: Copy + Float,
{
    length: T,
}

impl<T> Line<T>
where
    T: Copy + Float,
{
    #[inline(always)]
    pub fn new(length: T) -> Self {
        Line { length: length }
    }

    #[inline(always)]
    pub fn length(&self) -> T {
        self.length
    }
}

impl<T, B> Shape2D<T, B> for Line<T>
where
    T: 'static + Copy + Float,
    B: 'static + BoundingVolume2D<T>,
{
    #[inline(always)]
    fn area(&self) -> T {
        self.length
    }
    #[inline(always)]
    fn inertia(&self) -> T {
        (self.length * self.length) / T::from_usize(12)
    }
    #[inline]
    fn bounding_volume(&self, bounding_volume: &mut B, position: &[T; 2], rotation: T) {
        let mut tmp = vec2::create(self.length * T::from_f32(0.5), T::zero());

        if rotation != T::zero() {
            let t = tmp.clone();
            vec2::transform_angle(&mut tmp, &t, rotation);
        }

        let size = vec2::create(tmp[0].max(&-tmp[0]), tmp[1].max(&-tmp[1]));
        bounding_volume.from_center_size(position, &size);
    }
}
