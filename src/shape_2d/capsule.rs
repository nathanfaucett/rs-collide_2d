use number_traits::Float;
use vec2;

use super::super::BoundingVolume2D;
use super::Shape2D;

#[derive(Debug)]
pub struct Capsule<T>
where
    T: Copy + Float,
{
    radius: T,
    height: T,
}

impl<T> Capsule<T>
where
    T: Copy + Float,
{
    #[inline(always)]
    pub fn new(radius: T, height: T) -> Self {
        Capsule {
            radius: radius,
            height: height,
        }
    }

    #[inline(always)]
    pub fn radius(&self) -> T {
        self.radius
    }
    #[inline(always)]
    pub fn height(&self) -> T {
        self.height
    }
}

impl<T, B> Shape2D<T, B> for Capsule<T>
where
    T: 'static + Copy + Float,
    B: 'static + BoundingVolume2D<T>,
{
    #[inline(always)]
    fn area(&self) -> T {
        capsule_a(self.height, self.radius)
    }
    #[inline]
    fn inertia(&self) -> T {
        let area = capsule_a(self.height, self.radius);

        if area > T::zero() {
            capsule_i(self.height, self.radius) / area
        } else {
            T::zero()
        }
    }
    /// ```
    /// extern crate collide_2d;
    /// extern crate aabb2;
    ///
    /// use aabb2::AABB2;
    /// use collide_2d::{Shape2D, Capsule};
    ///
    /// fn main() {
    ///     let capsule = Capsule::new(0.5, 1.0);
    ///     let mut bv = AABB2::default();
    ///     capsule.bounding_volume(&mut bv, &[0.0, 0.0], 0.0);
    ///     assert_eq!(&bv.min, &[-1.0, -0.5]);
    ///     assert_eq!(&bv.max, &[1.0, 0.5]);
    /// }
    /// ```
    #[inline]
    fn bounding_volume(&self, bounding_volume: &mut B, position: &[T; 2], rotation: T) {
        let mut tmp = vec2::create(self.height * T::from_f32(0.5), T::zero());

        if rotation != T::zero() {
            let t = tmp.clone();
            vec2::transform_angle(&mut tmp, &t, rotation);
        }

        let r = self.radius;
        let size = vec2::create(
            (tmp[0] + r).max(&(-tmp[0] + r)) * T::from_usize(2),
            (tmp[1] + r).max(&(-tmp[1] + r)) * T::from_usize(2),
        );

        bounding_volume.from_center_size(position, &size);
    }
}

#[inline(always)]
fn box_i<T>(w: T, h: T) -> T
where
    T: Copy + Float,
{
    w * h * ((w * w) + (h * h)) / T::from_usize(12)
}

#[inline(always)]
fn semi_a<T>(r: T) -> T
where
    T: Copy + Float,
{
    T::PI() * (r * r) / T::from_usize(2)
}

#[inline(always)]
fn semi_i<T>(r: T) -> T
where
    T: Copy + Float,
{
    ((T::PI() / T::from_usize(4)) - (T::from_usize(8) / (T::from_usize(9) * T::PI())))
        * (r * r * r * r)
}

#[inline(always)]
fn semi_c<T>(r: T) -> T
where
    T: Copy + Float,
{
    (T::from_usize(4) * r) / (T::from_usize(3) * T::PI())
}

#[inline(always)]
fn capsule_a<T>(h: T, r: T) -> T
where
    T: Copy + Float,
{
    h * T::from_usize(2) * r + T::PI() * r * r
}

#[inline(always)]
fn capsule_i<T>(h: T, r: T) -> T
where
    T: Copy + Float,
{
    let d = h / T::from_usize(2) + semi_c(r);
    box_i(h, T::from_usize(2) * r) + T::from_usize(2) * (semi_i(r) + semi_a(r) * (d * d))
}
