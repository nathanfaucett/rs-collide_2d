#![feature(get_type_id)]

extern crate aabb2;
extern crate number_traits;
extern crate polygon2;
extern crate vec2;

mod broad_phase_2d;
mod bounding_volume_2d;
mod near_phase_2d;
mod query_2d;
mod shape_2d;
mod contact_2d;

pub use self::broad_phase_2d::*;
pub use self::bounding_volume_2d::*;
pub use self::near_phase_2d::*;
pub use self::query_2d::*;
pub use self::shape_2d::*;
pub use self::contact_2d::*;
