use std::hash::Hash;

use number_traits::Float;

use super::super::{circle_to_capsule, circle_to_circle, circle_to_convex, circle_to_line,
                   BoundingVolume2D, Capsule, Circle, Contact2D, Convex, Line, Shape2D};

pub trait NearPhase2D<I, T, B>: Default
where
    I: Copy + Hash + Ord + Eq,
    T: 'static + Copy + Float,
    B: 'static + BoundingVolume2D<T>,
{
    #[inline]
    fn run(
        &mut self,
        shapes: &[(I, &[T; 2], T, &B, &Box<Shape2D<T, B>>)],
        pairs: &[((I, usize), (I, usize))],
    ) -> Vec<Contact2D<I, T>> {
        let mut contacts = Vec::new();

        for &((id_i, index_i), (id_j, index_j)) in pairs {
            let &(_, xi, wi, _, si) = &shapes[index_i];
            let &(_, xj, wj, _, sj) = &shapes[index_j];
            let mut swap = false;

            if let Some((d, p, n)) = if let Some(ci) = si.downcast_ref::<Circle<T>>() {
                if let Some(cj) = sj.downcast_ref::<Circle<T>>() {
                    circle_to_circle(&xi, ci.radius(), &xj, cj.radius())
                } else if let Some(cj) = sj.downcast_ref::<Capsule<T>>() {
                    let (ls, le) = line_start_end(xj, wj, cj.height());
                    circle_to_capsule(&xi, ci.radius(), &ls, &le, cj.radius())
                } else if let Some(lj) = sj.downcast_ref::<Line<T>>() {
                    let (ls, le) = line_start_end(xj, wj, lj.length());
                    circle_to_line(&xi, ci.radius(), &ls, &le)
                } else if let Some(cj) = sj.downcast_ref::<Convex<T>>() {
                    circle_to_convex(&xi, ci.radius(), &xj, wj, cj.vertices())
                } else {
                    None
                }
            } else if let Some(li) = si.downcast_ref::<Line<T>>() {
                let (ls, le) = line_start_end(xi, wi, li.length());

                if let Some(cj) = sj.downcast_ref::<Circle<T>>() {
                    swap = true;
                    circle_to_line(&xj, cj.radius(), &ls, &le)
                } else {
                    None
                }
            } else if let Some(ci) = si.downcast_ref::<Capsule<T>>() {
                let (ls, le) = line_start_end(xi, wi, ci.height());

                if let Some(cj) = sj.downcast_ref::<Circle<T>>() {
                    swap = true;
                    circle_to_capsule(&xj, cj.radius(), &ls, &le, ci.radius())
                } else {
                    None
                }
            } else if let Some(ci) = si.downcast_ref::<Convex<T>>() {
                if let Some(cj) = sj.downcast_ref::<Circle<T>>() {
                    swap = true;
                    circle_to_convex(&xj, cj.radius(), &xi, wi, ci.vertices())
                } else {
                    None
                }
            } else {
                None
            } {
                contacts.push(if swap {
                    Contact2D::new(id_j, index_j, id_i, index_i, d, n, p)
                } else {
                    Contact2D::new(id_i, index_i, id_j, index_j, d, n, p)
                });
            }
        }

        contacts
    }
}

fn line_start_end<T>(lx: &[T; 2], lw: T, lh: T) -> ([T; 2], [T; 2])
where
    T: Copy + Float,
{
    let x = lw.cos();
    let y = lw.sin();
    let hl = lh * T::from_f32(0.5);
    (
        [lx[0] + x * -hl, lx[1] + y * -hl],
        [lx[0] + x * hl, lx[1] + y * hl],
    )
}
