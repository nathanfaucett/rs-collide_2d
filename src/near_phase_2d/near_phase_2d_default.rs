use std::hash::Hash;
use std::marker::PhantomData;

use number_traits::Float;

use super::super::BoundingVolume2D;
use super::NearPhase2D;

pub struct NearPhase2DDefault<I, T, B>
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
    B: BoundingVolume2D<T>,
{
    _marker: PhantomData<(I, T, B)>,
}

unsafe impl<I, T, B> Send for NearPhase2DDefault<I, T, B>
where
    I: Send + Copy + Hash + Ord + Eq,
    T: Send + Copy + Float,
    B: Send + BoundingVolume2D<T>,
{
}
unsafe impl<I, T, B> Sync for NearPhase2DDefault<I, T, B>
where
    I: Sync + Copy + Hash + Ord + Eq,
    T: Sync + Copy + Float,
    B: Sync + BoundingVolume2D<T>,
{
}

impl<I, T, B> Default for NearPhase2DDefault<I, T, B>
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
    B: BoundingVolume2D<T>,
{
    #[inline(always)]
    fn default() -> Self {
        NearPhase2DDefault {
            _marker: PhantomData,
        }
    }
}

impl<I, T, B> NearPhase2DDefault<I, T, B>
where
    I: Copy + Hash + Ord + Eq,
    T: Copy + Float,
    B: BoundingVolume2D<T>,
{
    #[inline(always)]
    pub fn new() -> Self {
        Self::default()
    }
}

impl<I, T, B> NearPhase2D<I, T, B> for NearPhase2DDefault<I, T, B>
where
    I: Copy + Hash + Ord + Eq,
    T: 'static + Copy + Float,
    B: 'static + BoundingVolume2D<T>,
{
}

#[cfg(test)]
mod test {
    use super::*;

    use aabb2::{self, AABB2};
    use vec2;

    use {BroadPhase2D, BroadPhase2DAABB2, Circle, Line, Shape2D};

    fn create_circle<T>(
        id: usize,
        position: [T; 2],
        shape: Circle<T>,
    ) -> (usize, [T; 2], T, AABB2<T>, Box<Shape2D<T, AABB2<T>>>)
    where
        T: 'static + Send + Sync + Copy + Float,
    {
        let mut aabb = aabb2::new_identity();
        let r = shape.radius();
        vec2::set(&mut aabb.min, position[0] - r, position[1] - r);
        vec2::set(&mut aabb.max, position[0] + r, position[1] + r);
        (id, position, T::zero(), aabb, Box::new(shape))
    }

    fn create_line<T>(
        id: usize,
        position: [T; 2],
        shape: Line<T>,
    ) -> (usize, [T; 2], T, AABB2<T>, Box<Shape2D<T, AABB2<T>>>)
    where
        T: 'static + Send + Sync + Copy + Float,
    {
        let mut aabb = aabb2::new_identity();
        let l = shape.length();
        vec2::set(&mut aabb.min, position[0] - l, position[1]);
        vec2::set(&mut aabb.max, position[0] + l, position[1]);
        (id, position, T::zero(), aabb, Box::new(shape))
    }

    #[test]
    fn test() {
        let mut broad_phase = BroadPhase2DAABB2::new();
        let mut near_phase = NearPhase2DDefault::new();

        let owned_objects = vec![
            create_circle(10, [0.0, 0.0], Circle::new(0.75)),
            create_circle(20, [0.0, 1.0], Circle::new(0.75)),
            create_line(30, [0.0, 4.0], Line::new(1.0)),
            create_circle(40, [0.0, 4.5], Circle::new(0.75)),
        ];
        let objects: Vec<_> = owned_objects
            .iter()
            .map(|&(id, ref position, rotation, ref aabb, ref shape)| {
                (id, position, rotation, aabb, shape)
            })
            .collect();

        let collisions = broad_phase.run(&*objects);

        assert_eq!(collisions.len(), 2);

        let contacts = near_phase.run(&*objects, &*collisions);

        let contact = &contacts[0];
        assert_eq!((contact.id_i(), contact.index_i()), (20, 1));
        assert_eq!((contact.id_j(), contact.index_j()), (10, 0));
        assert_eq!(contact.point(), &[0.0, 0.25]);
        assert_eq!(contact.normal(), &[0.0, -1.0]);
        assert_eq!(contact.depth(), 0.5);

        let contact = &contacts[1];
        assert_eq!((contact.id_i(), contact.index_i()), (40, 3));
        assert_eq!((contact.id_j(), contact.index_j()), (30, 2));
        assert_eq!(contact.point(), &[0.0, 3.75]);
        assert_eq!(contact.normal(), &[0.0, -1.0]);
        assert_eq!(contact.depth(), 0.25);
    }
}
